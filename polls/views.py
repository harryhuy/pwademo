from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.template import loader
from django.shortcuts import render

from .models import Question

# Create your views here.

def index(request):
    question_list = Question.objects.all()[:10]
    template = loader.get_template('polls/index.html')
    context = {
        'question_list': question_list
    }
    return HttpResponse(template.render(context, request))


def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404('Question doesn\'t exist.')
    return render(request, 'polls/details.html', {'question': question})


def results(request, question_id):
    return HttpResponse('You are looking at the results of question %s.' % question_id)


def vote(request, question_id):
    return HttpResponse('You are voting on question %s.' % question_id)
