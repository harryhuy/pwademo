# Possessive web app demo project

Powered by

* Django

* Webpack

* AdminLTE theme (https://adminlte.io/)

## How to run

```
# Install dependencies
pip install -r requerements.txt

# Build frontend script
cd home/static/dev
npm install
npm run build

# Run development mode
cd ../../../
python manage.py runserver
```
