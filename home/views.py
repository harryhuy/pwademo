from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.generic import TemplateView

from os import path

from PWADemo.settings import VERSION, BASE_DIR

# Create your views here.

def index(request):
    return render(request, 'home/index.html')


class ServiceWorkerView(TemplateView):
    template_name = 'sw.js'
    content_type = 'application/javascript'
    name = 'service_worker'

    def get_context_data(self, **kwargs):
        return {'VERSION': VERSION}


def offline(request):
    return render(request, 'home/index.html')


def favicon(request):
    file_path = path.join(BASE_DIR, 'home/static/home/img/favicon.ico')
    try:
        with open(file_path, 'rb') as icon_file:
            response = HttpResponse(icon_file.read(), content_type='image/vnd.microsoft.icon')
            return response
    except:
        return Http404()