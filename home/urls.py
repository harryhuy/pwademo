from django.urls import include, path
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('sw.js', views.ServiceWorkerView.as_view(), name=views.ServiceWorkerView.name),
    path('offline', views.offline, name='offline'),
    path('favicon.ico', views.favicon, name='favicon')
]
